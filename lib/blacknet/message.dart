import 'dart:typed_data'; 
import 'dart:convert';
import 'package:blacknet_lib/blacknet/utils.dart';

class Message {
    int type;
    String message;

    Message(this.type, this.message);
    Message.empty(){
      this.type = Message.plain;
    }
    serialize(){
      // type
      Uint8List type = Utils.toUint8(this.type);
      // message
      Uint8List msg = new Uint8List(0);
      if(this.message!=null){
        // msg = Uint8List.fromList(this.message.codeUnits);
        msg = Uint8List.fromList(utf8.encode(this.message));
      }
      // message length
      Uint8List len = encodeVarInt(msg.length);
      List<int> bytes = List<int>();
      bytes.addAll(type.toList());
      bytes.addAll(len.toList());
      bytes.addAll(msg.toList());
      return Uint8List.fromList(bytes);
    }
    static Message derialize(Uint8List arr){
      ByteData bytes = arr.buffer.asByteData();
      int type = bytes.getUint8(0);
      String message = utf8.decode(arr.sublist(arr.length - decodeVarInt(arr.sublist(1))).toList());
      return new Message(type, message);
    }
    static int plain = 0;
    static int encrypted = 1;
}